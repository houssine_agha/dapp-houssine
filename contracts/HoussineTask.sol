// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

/** 
 * @title Smart contract task for Con3x company
 * @dev A smart contract in Solidity that saves and reads a message.
 */
contract Houssine {

    struct Memo {
        string name;
        string message;
        uint timestamp;
        address from;
    }

    Memo[] memos ;
    address payable owner;
    constructor(){
        owner = payable (msg.sender);
    }
    
    function buy(string calldata name , string calldata message) external payable{
        require(msg.value>0 ,"please pay more than 0 Ethr");
        owner.transfer(msg.value);
        memos.push(Memo(name,message,block.timestamp,msg.sender));
    }

    function getData() public view returns(Memo[] memory) {
        return memos;
    }
}